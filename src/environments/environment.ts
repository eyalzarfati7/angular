// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyD0Fan_gTQ6ItAMT_w-beH0Dpnx3Im3Pkk",
    authDomain: "hello-c7203.firebaseapp.com",
    databaseURL: "https://hello-c7203.firebaseio.com",
    projectId: "hello-c7203",
    storageBucket: "hello-c7203.appspot.com",
    messagingSenderId: "8346698394",
    appId: "1:8346698394:web:0b02475abe4295de3b9523"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
